# WebRTC-Web

Simple video chat web application created with WebRTC and React.  
Application builds can be tested at: https://webrtc-multiplatform-master.herokuapp.com/  

How to deploy:

-Open console in project folder  
-Type "npm i" to download required node modules  
-Type "npm run build"  
-Generated build folder drop to the server project folder  
