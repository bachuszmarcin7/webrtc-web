import React, { useState, useEffect } from 'react';
import { useLocation } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';

import Notification from './utilities/Notification';
import UsersTable from "./lobby_components/LobbyUsersTable";
import CreateRoom from "./lobby_components/LobbyCreateRoom";
import JoinRoom from "./lobby_components/LobbyJoinRoom";

export default function Lobby(props)
{
    const [getUsersList, setUsersList] = useState([]); //Array that holds all users connected to server
	const [getNotificationComponent, setNotificationComponent] = useState(<Notification notification={0}/>);

    const classes = useStyles();
    const location = useLocation();

    var socket = props.socket;
    var userName = location.state.userName;

    useEffect(() =>
    {
        handleMessage();

        socket.emit("getUsersOnline");

        return () =>
        {
            socket.off("usersOnline");
            socket.off("userConnected");
            socket.off("userDisconnected");
        }
    }, []);

    useEffect(() =>
    {
        socket.on("userLeftRoom", userData => { updateUser(userData); });
        socket.on("userJoinedRoom", userData => { updateUser(userData); });
        socket.on("userCreatedRoom", userData => { updateUser(userData); });

        return () =>
        {
            socket.off("userJoinedRoom");
            socket.off("userCreatedRoom");
            socket.off("userLeftRoom");
        }
    }, [getUsersList]);

	useEffect(() => { }, [getNotificationComponent]);

    function handleMessage()
    {
        socket.on("usersOnline", users => { addUsersToList(users); });
        socket.on("userConnected", newUser => { addUserToList(newUser); });
        socket.on("userDisconnected", userSocket => {removeUserFromList(userSocket)});
    }

    function addUsersToList(users)
    {
        let clients = Object.values(users);

        clients.forEach((user) =>
        {
            if (user.socket === socket.id) { return; }

            setUsersList(getUsersList => [...getUsersList,
                {
                    socket: user.socket,
                    userName: user.name,
                    platform: user.platform,
                    room: user.room
                }]);
        });
    }

    function addUserToList(user)
    {
        setUsersList(getUsersList => [...getUsersList,
            {
                socket: user.socket,
                userName: user.name,
                platform: user.platform,
                room: user.room
            }]);
    }

    function updateUser(data)
    {
        let temp = [...getUsersList];

        temp.forEach(user =>
        {
            if (user.socket === data.socket)
            {
                user.room = data.room;
            }
        });

        setUsersList(temp);
    }

    function removeUserFromList(socketId)
    {
        setUsersList(getUsersList => getUsersList.filter(user => user.socket !== socketId));
    }

	function notificationCallback(action)
	{
		setNotificationComponent(<Notification notification={action}/>);
	}

    return (
        <div className={classes.root}>

            <UsersTable rows={getUsersList} />

            <CreateRoom socket={socket} userName={userName} callback={notificationCallback} />
            <JoinRoom socket={socket} userName={userName} callback={notificationCallback} />

			{getNotificationComponent}

        </div>
    );
}

const useStyles = makeStyles(
    {
        root:
        {
            position: "fixed",
            top: "0px",
            left: "0px",
            width: "100%",
            height: "100%",
            background: "linear-gradient(45deg, #4E11D4 30%, #7539F8 90%)",
        },
    });