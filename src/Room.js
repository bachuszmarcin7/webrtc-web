import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useLocation } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';

import UserMedia from "./room_components/UserMedia";
import RoomInterface from "./room_components/RoomInterface";
import PeerConnection from "./room_components/PeerConnection";

export default function Room(props)
{
    const [getStatus, setStatus] = useState("closed");
    const [getStreamMode, setStreamMode] = useState(0);
    const [getLocalStream, setLocalStream] = useState(null);
    const [getPeerConnections, setPeerConnections] = useState([]);

    const classes = useStyles();
    const history = useHistory();
    const location = useLocation();

    var socket = props.socket;
    var platform = props.platform;
    var userName = location.state.userName;
    var createdRoom = location.state.createdRoom;

    var rootContainer;
    var mediaContainer;
    var localVideo;

    // #region useEffect

    useEffect(() => { setStream(); }, [getStreamMode]);
    useEffect(() => { getUsersInRoom(); }, [getStatus]);
    useEffect(() => { addLocalTracks(); }, [getPeerConnections]);

    useEffect(() =>
    {
        handleMessage();

        return () =>
        {
            socket.emit("leave");
        }
    }, []);

    useEffect(() =>
    {
        handleMedia();

        return () =>
        {
            stopStream();
        }
    }, [getLocalStream]);

    //#endregion ----------------------------------------------------------------------------------------------------------

    // #region Media

    function handleMedia() //Handle value returned by UserMedia
    {
        if (getLocalStream === -1)
        {
            history.goBack();
        }
        else
        {
            applyStream();
        }
    }

    function setStream() //Get media stream from user devices and set it to 'getLocalStream' state
    {
        UserMedia(getStreamMode).then(stream => setLocalStream(stream));
    }

    function applyStream() //Whenever user stream change, update all peer connections with new stream
    {
        if (getLocalStream)
        {
            localVideo.srcObject = getLocalStream;

            getPeerConnections.forEach(peerConnection =>
            {
                peerConnection.props.peerConnection.getSenders().forEach(sender =>
                {
                    if (sender.track.kind === "video")
                    {
                        sender.replaceTrack(getLocalStream.getVideoTracks()[0]);
                    }
                });
            });

            if (getStatus == "closed") { setStatus("open"); }
        }
    }

    function stopStream() //Stop all user streams
    {
        if (getLocalStream)
        {
            getLocalStream.getTracks().forEach(track => track.stop());
        }
    }

    //#endregion ----------------------------------------------------------------------------------------------------------

    // #region PeerConnection

    async function createPeerConnection(userId) //Create new peer connection and store it in 'getPeerConnections' state
    {
        try
        {
            let peerConnectionComponent;
            let peerConnection = new RTCPeerConnection(
                {
                    sdpSemantics: "unified-plan",
                    iceServers: [{ "urls": "stun:stun.l.google.com:19302" }]
                });

            peerConnectionComponent = <PeerConnection
                userId={userId}
                socket={socket}
                platform={platform}
                peerConnection={peerConnection}/>;

            setPeerConnections(getPeerConnections => [...getPeerConnections, peerConnectionComponent]);
        }
        catch (exception)
        {
            console.error('Failed to create PeerConnection: ' + exception.message);
            return;
        }
    }

    function addLocalTracks() //Add stream to new peer connection
    {
        if (getLocalStream && getPeerConnections && getPeerConnections.length !== 0)
        {
            try
            {
                let index = getPeerConnections.length - 1;

                if (getPeerConnections[index].props.peerConnection.connectionState === "new")
                {
                    getLocalStream.getTracks().forEach(track =>
                    {
                        getPeerConnections[index].props.peerConnection.addTrack(track, getLocalStream);
                    });
                }
            }
            catch (exception)
            {
                console.error(exception);
                return;
            }
        }
    }

    //#endregion ----------------------------------------------------------------------------------------------------------

    // #region Socket

    function getUsersInRoom() //Request list of users in room
    {
        if (createdRoom === false && getStatus === "open")
        {
            socket.emit("getRoomUsers");
        }
    }

    function handleMessage()
    {
        socket.on("usersList", message => //Based on received list of users in room, create peer connection for each user
        {
            Object.keys(message).forEach(user =>
            {
                if (user !== socket.id)
                {
                    createPeerConnection(user);
                }
            });
        });

        socket.on("messageToRoom", message => //When a new user join room create peer connection
        {
            if (message.type === "userJoined")
            {
                createPeerConnection(message.user);
            }
        });
    }

    //#endregion ----------------------------------------------------------------------------------------------------------

    // #region Callbacks

    function interfaceCallback(action)
    {
        if (action === 0) //Return to previous page
        {
            history.goBack(); 
        }
        else if (action === 1) //Disable audio stream
        {
			if(getLocalStream.getAudioTracks()[0])
			{
				getLocalStream.getAudioTracks()[0].enabled = !(getLocalStream.getAudioTracks()[0].enabled); 
			}
        }
        else if (action === 2) //Disable camera/desktop stream
        {
			if(getLocalStream.getVideoTracks()[0])
			{
				getLocalStream.getVideoTracks()[0].enabled = !(getLocalStream.getVideoTracks()[0].enabled); 
			}
        }
        else if (action === 3) //Change stream mode betwen camera/desktop
        {
            setStreamMode(1 - getStreamMode); 
        }
        else if (action === 4) //Change size of media container whenever chat is open/closed
        {
            let rootWidth = rootContainer.getBoundingClientRect().width;
            let mediaWidth = mediaContainer.getBoundingClientRect().width;

            if (mediaWidth === rootWidth)
            {
                mediaContainer.style.setProperty("width", 80 + "%");
            }
            else
            {
                mediaContainer.style.setProperty("width", 100 + "%");
            }
        }
    }

    //#endregion ----------------------------------------------------------------------------------------------------------

    return (
        <div className={classes.root} ref={(ref) => rootContainer = ref}>

            <div className={classes.mediaContainer} ref={(ref) => mediaContainer = ref}>

                <div className={classes.media}>
                    <video className={classes.video} ref={(ref) => localVideo = ref} autoPlay playsInline muted></video>
                </div>

                {getPeerConnections}

            </div>

            <RoomInterface socket={props.socket} userName={userName} callback={interfaceCallback} />

        </div>
        );
}

const useStyles = makeStyles(
    {

        root:
        {
            position: "absolute",
            top: "0px",
            left: "0px",
            width: "100%",
            height: "100%",
        },
        mediaContainer:
        {
            width: "100%",
            height: "100%",
            display: "flex",
            flexWrap: "wrap",
            background: "linear-gradient(45deg, #4E11D4 30%, #7539F8 90%)",
        },
        media:
        {
            position: "relative",
            flex: "1 0 50%",
        },
        video:
        {
            position: "absolute",
            top: "0",
            left: "0",
            width: "100%",
            height: "100%",
            objectFit: "fill"
        },
    });