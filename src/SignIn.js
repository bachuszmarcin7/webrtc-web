import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Notification from './utilities/Notification';
import SignInEnterName from "./signIn_components/SignInEnterName";

export default function SignIn(props)
{
	const [getNotificationComponent, setNotificationComponent] = useState(<Notification notification={0}/>);

    const classes = useStyles();

    useEffect(() =>
    {
        socket.emit("userDisconnected");
    }, []);

	useEffect(() => { }, [getNotificationComponent]);

    var socket = props.socket;
    var platform = props.platform;

	function notificationCallback(action)
	{
		setNotificationComponent(<Notification notification={action}/>);
	}

    return (
        <div className={classes.root}>
            <SignInEnterName socket={socket} platform={platform} callback={notificationCallback} />
			{getNotificationComponent}
        </div>
    );
}

const useStyles = makeStyles(
    {
        root:
        {
            position: "fixed",
            top: "0px",
            left: "0px",
            width: "100%",
            height: "100%",
            background: "linear-gradient(45deg, #4E11D4 30%, #7539F8 90%)",
        },
    });