import React from 'react';
import { render } from 'react-dom';
import { browserName } from 'react-device-detect';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import socketIOClient from "socket.io-client";

import SignIn from "./SignIn";
import Lobby from "./Lobby";
import Room from "./Room";

var platform = browserName; //User platform used to adjust SDP to work with other platforms

var socket = socketIOClient("https://localhost:443/", { secure: true, forceNew: true, transports: ["websocket"], upgrade: false, });

const App = () => (
    <Router>
        <Switch>

            <Route exact path="/">
                <SignIn socket={socket} platform={platform} />
            </Route>

            <Route path="/lobby">
                <Lobby socket={socket} />
            </Route>

            <Route path="/room/:roomName">
                <Room socket={socket} platform={platform} />
            </Route>

        </Switch>
    </Router>
);

render(<App />, document.getElementById('root'));
