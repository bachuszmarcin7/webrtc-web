import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

export default function CreateRoomComponent(props)
{
    const classes = useStyles();
    const history = useHistory();

    var socket = props.socket
    var userName = props.userName;
    var validInput = true;

    let roomNameInput = null;
    let roomLimitInput = null;

    useEffect(() =>
    {
        handleMessage();

        return () =>
        {
            socket.off("roomCreated");
            socket.off("roomExists");
        }
    }, []);

    function handleMessage()
    {
        socket.on("roomCreated", roomName =>
        {
            history.push({ pathname: `/room/${roomName}`, state: { userName: userName, roomName: roomName, createdRoom: true } });
        });

        socket.on("roomExists", roomName => { props.callback(4); });
    }

    function handleOnClick()
    {
        let roomName = roomNameInput.value;
        let maxUsers = roomLimitInput.value;

        if (roomName !== "")
        {
            if (maxUsers !== "" && maxUsers > 0)
            {
                if (validInput === true)
                {
                    socket.emit("roomCreate", { roomName: roomName, maxUsers: maxUsers });
                }
            }
        }
		else { props.callback(3); }
    }

    function handleInput(event)
    {
        validInput = event.target.validity.valid
    }

    return (
        <div className={classes.root}>

            <TextField className={classes.text} value="Max Users" InputProps={{ disableUnderline: true, readOnly: true }} />
            <input className={classes.roomLimitInput} maxLength="2" type="text" pattern="[0-9]*" defaultValue="2" ref={(input) => { roomLimitInput = input; }} onChange={handleInput} />

            <TextField className={classes.text} value="Room Name" InputProps={{ disableUnderline: true, readOnly: true }}/>
            <input className={classes.roomNameInput} maxLength="30" type="text" ref={(input) => { roomNameInput = input; }} />
            <button className={classes.button} onClick={handleOnClick}>Create</button>

        </div>
    );
}

const useStyles = makeStyles(
    {
        root:
        {
            position: "relative",
            padding: "1%",
            top: "0px",
            left: "0px",
            width: "30%",
            boxShadow: "5px 10px 18px #343434",
            background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        },
        text:
        {
            "& .MuiInputBase-root":
            {
                fontWeight: "bold",
            },
        },
        roomNameInput:
        {
            position: "relative",
            width: "74%",
            height: "23px",
            paddingLeft: "5px",
            fontSize: "1rem",
            fontFamily: "Roboto",
            fontWeight: "400",
            lineHeight: "1.1876em",
            letterSpacing: "0.00938em",
            color: "black",
            background: 'linear-gradient(45deg, #FF9EBE 30%, #FFC186 90%)',
            borderRadius: 10,
            border: "0px",
        },
        roomLimitInput:
        {
            position: "relative",
            width: "74%",
            height: "23px",
            paddingLeft: "5px",
            fontSize: "1rem",
            fontFamily: "Roboto",
            fontWeight: "400",
            lineHeight: "1.1876em",
            letterSpacing: "0.00938em",
            color: "black",
            background: 'linear-gradient(45deg, #FF9EBE 30%, #FFC186 90%)',
            borderRadius: 10,
            border: "0px",
        },
        button:
        {
            position: "relative",
            left: "3.5%",
            width: "20%",
            height: "23px",
            fontSize: "1rem",
            fontFamily: "Roboto",
            fontWeight: "400",
            lineHeight: "1.1876em",
            letterSpacing: "0.00938em",
            color: "white",
            background: 'linear-gradient(45deg, #161616 30%, #2D2D2D 90%)',
            borderRadius: 10,
            border: "0px",
        }
    });