import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const columns = [
    { id: "userName", label: "User Name", minWidth: 40 },
    { id: "platform", label: "Platform", minWidth: 40 },
    { id: "room", label: "Room", minWidth: 145 }
];

const useStyles = makeStyles(
    {
        root:
        {
            float: "right",
            position: "absolute",
            top: "0px",
            right: "0px",
            width: "25%",
            minHeight: "50%",
            boxShadow: "5px 10px 18px #343434",
            background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
        },
        table:
        {
            position: "relative",
            backgroundColor: "transparent",
        },
        container:
        {
            maxHeight: "483px",

            "& .MuiTableCell-root":
            {
                borderBottom: "none",
            },
            "& .MuiTableCell-head":
            {
                fontWeight: "bold",
                background: "#FE6B8B",
            },
            "& .MuiTableCell-body":
            {
                fontWeight: "500",
            },
        },
});

export default function UsersTableComponent(props)
{
    const classes = useStyles();
    const [page] = React.useState(0);
    const [rowsPerPage] = React.useState(50);

    return (
        <div className={classes.root}>

            <Paper className={classes.table}>
                <TableContainer className={classes.container}>
                    <Table stickyHeader aria-label="sticky table">

                        <TableHead>
                            <TableRow>
                                {
                                    columns.map((column) => (
                                        <TableCell key={column.id} align={column.align} style={{ minWidth: column.minWidth }}>

                                            {column.label}

                                        </TableCell>
                                    ))
                                }
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {
                                props.rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) =>
                                {
                                    return (
                                        <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                                            {
                                                columns.map((column) =>
                                                {
                                                    const value = row[column.id];
                                                    return (
                                                        <TableCell key={column.id} align={column.align}>

                                                            {column.format && typeof value === "number" ? column.format(value) : value}

                                                        </TableCell>
                                                    );
                                                })
                                            }
                                        </TableRow>
                                    );
                                })
                            }
                        </TableBody>

                    </Table>
                </TableContainer>
            </Paper>

        </div>
    );
}
