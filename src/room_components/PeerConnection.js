import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import RoomVideoPanel from "./RoomVideoPanel";

export default function PeerConnectionComponent(props)
{
    const [getAudioMuted, setAudioMuted] = useState(false);

    const classes = useStyles();

    var userId = props.userId;
    var socket = props.socket;
    var platform = props.platform;
    var peerConnection = props.peerConnection;

    var root;
    var video;
    var audio;

    useEffect(() =>
    {
        setup();

        return () =>
        {
            closePeerConnection();
        }
    }, []);

    useEffect(() => { }, [getAudioMuted]);

    function setup() // Setup socket and peer callbacks and make a call if user joined room
    {
        handleMessage();

        peerConnection.onicecandidate = event =>
        {
            if (event.candidate)
            {
                let candidate = { type: 'candidate', label: event.candidate.sdpMLineIndex, id: event.candidate.sdpMid, candidate: event.candidate.candidate };

                socket.emit("messageToUser", { user: userId, content: candidate });
            }
            else { console.log('End of candidates'); }
        };
        peerConnection.ontrack = event => 
        {
            if (event.track.kind === "audio") //Due to a sdp problems when connecting with unity client audio must me stored in separate element
            {
                audio.srcObject = event.streams[0];
                audio.onloadeddata = () => { audio.play(); }
            }
            else { video.srcObject = event.streams[0]; }
        };
        peerConnection.oniceconnectionstatechange = () =>
        {
            if (peerConnection.iceConnectionState === 'disconnected')
            {
                closePeerConnection();
            }
        }
        peerConnection.onnegotiationneeded = () => //In case where tracks were changed/added when connection was established
        {
            console.log("Need negotiation!");

            //peerConnection.restartIce();
            call();
        }

        console.log("Created peer connection");
    }

    async function setLocalDescriptionSucceed(sessionDescription)
    {
        await peerConnection.setLocalDescription(sessionDescription);

        Object.assign(sessionDescription, { platform: platform });

        socket.emit("messageToUser", { user: userId, content: sessionDescription });
    }

    function setLocalDescriptionFailure(exception)
    {
        console.error("Failed to set local description: " + exception.message)
    }

    async function call()
    {
        await peerConnection.createOffer(setLocalDescriptionSucceed, setLocalDescriptionFailure,
            {
                OfferToReceiveVideo: true,
                OfferToReceiveAudio: true,
            });
    }

    async function makeAnswer(remoteDescription)
    {
        await peerConnection.setRemoteDescription(new RTCSessionDescription(remoteDescription));
        await peerConnection.createAnswer(setLocalDescriptionSucceed, setLocalDescriptionFailure,
            {
                mandatory:
                {
                    OfferToReceiveVideo: true,
                    OfferToReceiveAudio: true,
                }
            });
    }

    function applyAnswer(remoteDescription)
    {
        peerConnection.setRemoteDescription(new RTCSessionDescription(remoteDescription));
    }

    function peerAddIceCandidate(iceCandidate)
    {
        var candidate = new RTCIceCandidate(
            {
                sdpMLineIndex: iceCandidate.label,
                candidate: iceCandidate.candidate
            });

        peerConnection.addIceCandidate(candidate);
    }

    function closePeerConnection()
    {
        console.log("Closing peer connection");

        if (peerConnection.connectionState !== "closed")
        {
            peerConnection.close();
            root.remove();
        }
    }

    function handleMessage()
    {
        socket.on("messageToUser", message =>
        {
            let user = message.user;
            let content = message.content;

            if (user !== userId) { return; }

            if (peerConnection.connectionState !== "closed")
            {
                if (content.type === "offer") { makeAnswer(content); }
                if (content.type === "answer") { applyAnswer(content); }
                if (content.type === "candidate") { peerAddIceCandidate(content); }
            }
        });

        socket.on("connectionState", message =>
        {
            let user = message.user;
            let content = message.content;

            if (user !== userId) { return; }

            if (peerConnection.connectionState !== "closed")
            {
                if (content.type === "connectionClosed") { closePeerConnection(); }
            }
        });
    }

    function panelCallback()
    {
        audio.muted = !audio.muted;
        setAudioMuted(!getAudioMuted);
    }

    return (
        <div className={classes.media} ref={(ref) => root = ref}>

            <video className={classes.video} ref={(ref) => video = ref} autoPlay playsInline muted/>
            <audio ref={(ref) => audio = ref} />

            <RoomVideoPanel isAudioMuted={getAudioMuted} callback={panelCallback} />

        </div>
    );
}

const useStyles = makeStyles(
    {
        media:
        {
            position: "relative",
            flex: "1 0 50%",
        },
        video:
        {
            position: "absolute",
            top: "0",
            left: "0",
            width: "100%",
            height: "100%",
            objectFit: "fill"
        },
    });