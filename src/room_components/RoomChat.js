import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

export default function RoomChatComponent(props)
{
    const classes = useStyles();

    var messageInput = null;

    const handleKeyPress = (event) =>
    {
        if (event.key === 'Enter')
        {
            handleOnClick();
        }
    }

    const handleOnClick = () =>
    {
        if (messageInput.value !== "")
        {
            props.sendMessage(messageInput.value);
            messageInput.value = "";
        }
    }

    function Chat()
    {
        if (props.isHidden) { return null; }
        return (
            <div className={classes.root}>

                <div className={classes.messages}>

                    {props.messages}

                </div>

                <input className={classes.input} type="text" ref={(input) => { messageInput = input; }} onKeyPress={handleKeyPress} autoFocus />
                <button className={classes.button} onClick={handleOnClick}>Send</button>

            </div>
        );
    }

    return (
        <Chat/>
    );
}

const useStyles = makeStyles(
    {
        root:
        {
            float: "right",
            position: "absolute",
            top: "0px",
            right: "0px",
            width: "20%",
            height: "100%",
            background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        },
        messages:
        {
            float: "right",
            position: "relative",
            width: "100%",
            height: "94%",
            overflow: "auto",
        },
        input:
        {
            position: "relative",
            top: "1.4%",
            left: "3%",
            width: "65%",
            height: "27px",
            paddingLeft: "5px",
            fontSize: "1rem",
            fontFamily: "Roboto",
            fontWeight: "400",
            lineHeight: "1.1876em",
            letterSpacing: "0.00938em",
            color: "black",
            background: 'linear-gradient(45deg, #FF9EBE 30%, #FFC186 90%)',
            borderRadius: 10,
            border: "0px",
        },
        button:
        {
            position: "relative",
            top: "1.4%",
            left: "5%",
            width: "24%",
            height: "29px",
            fontSize: "1rem",
            fontFamily: "Roboto",
            fontWeight: "400",
            lineHeight: "1.1876em",
            letterSpacing: "0.00938em",
            color: "white",
            background: 'linear-gradient(45deg, #161616 30%, #2D2D2D 90%)',
            borderRadius: 10,
            border: "0px",
        }
    });