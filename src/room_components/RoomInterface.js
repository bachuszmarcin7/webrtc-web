import React, { useEffect, useState } from 'react';

import RoomChat from "./RoomChat";
import RoomPanel from "./RoomPanel";
import LocalMessage from "./RoomLocalMessage";
import RemoteMessage from "./RoomRemoteMessage";

export default function RoomInterfaceComponent(props)
{
    const [getMessages, setMessages] = useState([]);
    const [getMicrophoneMuted, setMicrophoneMuted] = useState(false);
    const [getCameraDisabled, setCameraDisabled] = useState(false);
    const [getScreenShareDisabled, setScreenShareDisabled] = useState(false);
    const [getChatHidden, setChatHidden] = useState(true);

    var socket = props.socket;
    var userName = props.userName;

    useEffect(() => { handleMessage(); }, []);

    useEffect(() => { }, [getMessages, getMicrophoneMuted, getCameraDisabled]);

    function handleMessage()
    {
        socket.on("messageToRoom", message =>
        {
            if (message.type === "message")
            {
                setMessages(getMessages => [...getMessages, <RemoteMessage userName={message.user} message={message.content} />]);
            }
        });
    }

    function chatCallback(message)
    {
        setMessages(getMessages => [...getMessages, <LocalMessage userName={userName} message={message} />]);
        socket.emit("messageToRoom", { type: "message", user: userName, content: message });
    }

    function panelCallback(action)
    {
        if (action === 0) { props.callback(action); }
        if (action === 1)
        {
            props.callback(action);
            setMicrophoneMuted(!getMicrophoneMuted);
        }
        if (action === 2)
        {
            props.callback(action);
            setCameraDisabled(!getCameraDisabled);
        }
        if (action === 3)
        {
            props.callback(action);
            setScreenShareDisabled(!getScreenShareDisabled);
        }
        if (action === 4)
        {
            props.callback(action);
            setChatHidden(!getChatHidden);
        }
    }

    return (
        <div>

            <RoomPanel callback={panelCallback} isMicrophoneMuted={getMicrophoneMuted} isCameraEnabled={getCameraDisabled} isScreenShareDisabled={getScreenShareDisabled} />
            <RoomChat sendMessage={chatCallback} messages={getMessages} isHidden={getChatHidden} />

        </div>
        );
}