import { makeStyles } from '@material-ui/core/styles';

import Fab from '@material-ui/core/Fab';
import CallEndIcon from '@material-ui/icons/CallEnd';
import MicOffIcon from '@material-ui/icons/MicOff';
import MicIcon from '@material-ui/icons/Mic';
import VideocamOffIcon from '@material-ui/icons/VideocamOff';
import VideocamIcon from '@material-ui/icons/Videocam';
import ChatIcon from '@material-ui/icons/Chat';
import StopScreenShareIcon from '@material-ui/icons/StopScreenShare';
import ScreenShareIcon from '@material-ui/icons/ScreenShare';

export default function RoomPanelComponent(props)
{
    const classes = useStyles();

    function Stop() { props.callback(0); };
    function Microphone() { props.callback(1); };
    function Camera() { props.callback(2); };
    function ScreenShare() { props.callback(3); };
    function Chat() { props.callback(4); };

    function MicrophoneIcon()
    {
        if (props.isMicrophoneMuted) { return <MicOffIcon fontSize="large" style={{ color: "#000000" }} />; }
        return <MicIcon fontSize="large" style={{ color: "#000000" }} />;
    };

    function CameraIcon()
    {
        if (props.isCameraEnabled) { return <VideocamOffIcon fontSize="large" style={{ color: "#000000" }} />; }
        return <VideocamIcon fontSize="large" style={{ color: "#000000" }} />;
    };

    function ScreenIcon()
    {
        if (props.isScreenShareDisabled) { return <StopScreenShareIcon fontSize="large" style={{ color: "#000000" }} />; }
        return <ScreenShareIcon fontSize="large" style={{ color: "#000000" }} />;
    };

    return (
        <div className={classes.root}>

            <Fab className={classes.button} onClick={Stop}>
                <CallEndIcon fontSize="large" style={{ color: "#000000" }} />
            </Fab>

            <Fab className={classes.button} onClick={Microphone}>
                <MicrophoneIcon />
            </Fab>

            <Fab className={classes.button} onClick={Camera}>
                <CameraIcon />
            </Fab>

            <Fab className={classes.button} onClick={ScreenShare}>
                <ScreenIcon />
            </Fab>

            <Fab className={classes.button} onClick={Chat}>
                <ChatIcon fontSize="large" style={{ color: "#000000" }} />
            </Fab>

        </div>
        );
}

const useStyles = makeStyles(
    {
        root:
        {
            position: "absolute",
            left: "10px",
            bottom: "10px",
        },
        button:
        {
            position: "relative",
            marginLeft: "10px",
            background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        },
    });