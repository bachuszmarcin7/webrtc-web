import { styled } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';

export default function LocalMessageComponent(props)
{
    function getCurrentTime()
    {
        let today = new Date();

        let minutes;
        let hours;
        let date;

        if (today.getMinutes() < 10) { minutes = "0" + today.getMinutes(); }
        else { minutes = today.getMinutes(); }

        if (today.getHours() < 10) { hours = "0" + today.getHours(); }
        else { hours = today.getHours(); }

        date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        return date + "  " + hours + ':' + minutes;
    }

    return (
        <div className="remoteMessage">

            <Tooltip title={getCurrentTime()}>

                <RemoteMessage multiline label={props.userName} value={props.message} InputProps={{ disableUnderline: true, readOnly: true }} />

            </Tooltip>

        </div>
    );
}

const RemoteMessage = styled(TextField)(
    {
        top: "5px",
        float: "left",
        marginTop: "5px",
        marginLeft: "10px",
        boxShadow: "5px 10px 18px #343434",
        background: 'linear-gradient(45deg, #161616 30%, #2D2D2D 90%)',
        borderRadius: 10,

        "& .MuiFormLabel-root":
        {
            color: "white",
            paddingTop: "5px",
            paddingLeft: "5px",
        },
        "& .MuiInputBase-root":
        {
            color: "white",
            paddingLeft: "5px",
        }
    });