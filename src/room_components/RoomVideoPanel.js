import { makeStyles } from '@material-ui/core/styles';

import Fab from '@material-ui/core/Fab';
import VolumeOffIcon from '@material-ui/icons/VolumeOff';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';

export default function RoomVideoPanelComponent(props)
{
    const classes = useStyles();

    function Audio() { props.callback(); };

    function AudioIcon()
    {
        if (props.isAudioMuted) { return <VolumeOffIcon fontSize="large" style={{ color: "#000000" }} />; }
        return <VolumeUpIcon fontSize="large" style={{ color: "#000000" }} />;
    };

    return (
        <div className={classes.root}>

            <Fab className={classes.button} onClick={Audio}>
                <AudioIcon />
            </Fab>

        </div>
        );
}

const useStyles = makeStyles(
    {
        root:
        {
            position: "relative",
            left: "10px",
            top: "10px",
            width: "10%",
        },
        button:
        {
            position: "relative",
            marginLeft: "10px",
            background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        },
    });