export default async function UserMedia(mode = 0)
{
    try
    {
        if (mode === 0)
        {
            const stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
            return stream;
        }
        if (mode === 1)
        {
            const stream = await navigator.mediaDevices.getDisplayMedia({ video: true, audio: true });
            return stream;
        }
    }
    catch (exception)
    {
        if (exception.name === "NotAllowedError")
        {
            if (mode === 1) return UserMedia();

            else { return -1; }
        }
		if (exception.name === "NotFoundError")
		{
			try
			{
				if (mode === 0)
				{
					const stream = await navigator.mediaDevices.getUserMedia({ video: false, audio: true });
					return stream;
				}
			}
			catch (exception)
			{
				if (exception.name === "NotFoundError")
				{
					if (mode === 0)
					{
						const stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: false });
						return stream;
					}
				}
				else { HandleGetUserMediaException(exception) }
			}
		}
        else { HandleGetUserMediaException(exception) }
    }

	async function HandleGetUserMediaException(exception)
	{
		console.error(exception.message);
        console.log("Trying Again!");

		await new Promise(r => setTimeout(r, 3000));

        UserMedia(mode);
	}
}