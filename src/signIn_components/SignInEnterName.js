import React from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

export default function SignInEnterName(props)
{
    const classes = useStyles();
    const history = useHistory();

    var socket = props.socket;
    var platform = props.platform;

    let userNameInput = null;

    const handleKeyPress = (event) =>
    {
        if (event.key === 'Enter')
        {
            handleOnClick();
        }
    }

    function handleOnClick()
    {
        let userName = userNameInput.value;

        if (userName !== "")
        {
            history.push({ pathname: "/lobby", state: { userName: userName } });
            socket.emit("userConnected", { userName: userName, platform: platform });
        }
		else { props.callback(2); }
    }

    return (
        <div className={classes.root}>

            <TextField className={classes.text} value="User Name" InputProps={{ disableUnderline: true, readOnly: true }} />
            <input className={classes.input} maxLength="30" type="text" ref={(input) => { userNameInput = input; }} onKeyPress={handleKeyPress} autoFocus />
            <button className={classes.button} onClick={handleOnClick}>Sign In</button>

        </div>
    );
}

const useStyles = makeStyles(
    {
        root:
        {
            position: "relative",
            padding: "1%",
            top: "46%",
            left: "36.5%",
            width: "25%",
            boxShadow: "5px 10px 18px #343434",
            background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        },
        text:
        {
            "& .MuiInputBase-root":
            {
                fontWeight: "bold",
            },
        },
        input:
        {
            position: "relative",
            width: "70%",
            height: "23px",
            paddingLeft: "5px",
            fontSize: "1rem",
            fontFamily: "Roboto",
            fontWeight: "400",
            lineHeight: "1.1876em",
            letterSpacing: "0.00938em",
            color: "black",
            background: 'linear-gradient(45deg, #FF9EBE 30%, #FFC186 90%)',
            borderRadius: 10,
            border: "0px",
        },
        button:
        {
            position: "relative",
            marginLeft: "3.5%",
            width: "24%",
            height: "23px",
            fontSize: "1rem",
            fontFamily: "Roboto",
            fontWeight: "400",
            lineHeight: "1.1876em",
            letterSpacing: "0.00938em",
            color: "white",
            background: 'linear-gradient(45deg, #161616 30%, #2D2D2D 90%)',
            borderRadius: 10,
            border: "0px",
        }
    });