import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

export default function Notification(props)
{
	const [getNotificationText, setNotificationText] = useState("");

    const classes = useStyles();

	var root;

	useEffect(() => 
	{
		if(props.notification !== 0)
		{
			root.style.setProperty("visibility", "visible");

			if(props.notification === 1)
			{
				setNotificationText("Nie udalo polaczyc sie z serwerem");
			}
			if(props.notification === 2)
			{
				setNotificationText("Nieprawidlowa nazwa uzytkownika");
			}
			if(props.notification === 3)
			{
				setNotificationText("Nie mozna stowrzyc pokoju (Nieprawidlowa nazwa pokoju)");
			}
			if(props.notification === 4)
			{
				setNotificationText("Nie mozna stowrzyc pokoju (Pokoj o takiej nazwie juz istnieje)");
			}
			if(props.notification === 5)
			{
				setNotificationText("Nie mozna dolaczyc do pokoju (Nieprawidlowa nazwa pokoju)");
			}
			if(props.notification === 6)
			{
				setNotificationText("Nie mozna dolaczyc do pokoju (Pokoj nie istnieje)");
			}
			if(props.notification === 7)
			{
				setNotificationText("Nie mozna dolaczyc do pokoju (Pokoj jest pelny)");
			}
		}
	}, [props]);

	function handleOnClick()
    {
		root.style.setProperty("visibility", "hidden");
    }

    return (
        <div className={classes.root} ref={(ref) => root = ref}>
            <div className={classes.notification}>

				<div className={classes.textArea}>
					<TextField multiline className={classes.text} value={getNotificationText} InputProps={{ disableUnderline: true, readOnly: true }} />
				</div>

				<button className={classes.button} onClick={handleOnClick}>Ok</button>

			</div>
        </div>
    );
}

const useStyles = makeStyles(
    {
        root:
        {
            position: "absolute",
            top: "0px",
            left: "0px",
            width: "100%",
            height: "100%",
            background: "rgba(224, 224, 235, 0.4)",
			visibility: "hidden",
        },
		notification:
        {
            position: "relative",
            top: "35%",
            left: "35%",
            width: "30%",
            height: "30%",
            background: "rgb(224, 224, 235)",
        },
		textArea:
		{
			position: "relative",
            top: "10%",
            left: "5%",
            width: "90%",
            height: "72%",
		},
		text:
        {
			height: "100%",
			width: "100%",

            "& .MuiInputBase-root":
            {
                fontWeight: "bold",
            },
        },
		button:
        {
            position: "relative",
			top: "15%",
			left: "67.5%",
            marginLeft: "3.5%",
            width: "24%",
            height: "23px",
            fontSize: "1rem",
            fontFamily: "Roboto",
            fontWeight: "400",
            lineHeight: "1.1876em",
            letterSpacing: "0.00938em",
            color: "white",
            background: 'linear-gradient(45deg, #161616 30%, #2D2D2D 90%)',
            borderRadius: 10,
            border: "0px",
        }
    });